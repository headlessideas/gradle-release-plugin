@file:Suppress("UnstableApiUsage")

import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask

plugins {
    kotlin("jvm") version BuildPluginsVersion.KOTLIN
    id("io.gitlab.arturbosch.detekt") version BuildPluginsVersion.DETEKT
    id("com.github.ben-manes.versions") version BuildPluginsVersion.VERSIONS_PLUGIN
    `reckon-config`
    id("com.gradle.plugin-publish") version BuildPluginsVersion.PLUGIN_PUBLISH
    `kotlin-dsl`
}

group = "com.headlessideas"

repositories {
    google()
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib")
    implementation(gradleApi())
    implementation(Versioning.reckonCore)
    implementation(Versioning.jgit) {
        exclude(group = "org.apache.httpcomponents", module = "httpclient")
        exclude(group = "org.slf4j", module = "slf4j-api")
    }
    implementation(Versioning.jgitApache)

    testImplementation(Kotest.core)
    testImplementation(Kotest.runner)
    testImplementation(Testing.mockk)
    testImplementation(Testing.strikt)
}


kotlin {
    jvmToolchain(17)
}

gradlePlugin {
    website.set(PluginBundle.WEBSITE)
    vcsUrl.set(PluginBundle.VCS)
    plugins {
        create(PluginCoordinates.ID) {
            group = PluginCoordinates.GROUP
            id = PluginCoordinates.ID
            displayName = PluginBundle.DISPLAY_NAME
            description = PluginBundle.DESCRIPTION
            implementationClass = PluginCoordinates.IMPLEMENTATION_CLASS
            tags.set(PluginBundle.TAGS)
        }
    }
}

detekt {
    config = rootProject.files("config/detekt/detekt.yml")
}

tasks {
    register("printVersion") {
        println(project.version)
    }

    withType<DependencyUpdatesTask> {
        rejectVersionIf {
            isNonStable(candidate.version)
        }
    }

    test {
        useJUnitPlatform()
    }
}

fun isNonStable(version: String) = "^[0-9,.v-]+(-r)?$".toRegex().matches(version).not()
