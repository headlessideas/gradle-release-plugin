plugins {
    id("org.ajoberstar.reckon")
}

reckon {
    calcScopeFromProp()
    stages("rc", "final")
    calcStageFromProp()
}
