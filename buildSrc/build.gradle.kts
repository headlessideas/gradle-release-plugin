plugins {
    `kotlin-dsl`
}
repositories {
    mavenCentral()
}

dependencies {
    implementation("org.ajoberstar.reckon:org.ajoberstar.reckon.gradle.plugin:0.18.0")
}